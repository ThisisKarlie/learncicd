from unittest.mock import MagicMock
from DisasterDataApp.models.train_classifier import tokenize, load_data, build_model
# from DisasterDataApp.utils import tokenize

x, y, col_names = load_data('DisasterDataApp/data/myDatabase.db')
X_train, X_test, y_train, y_test = x[:10], x[-10:-9], y[:10], y[-10:-9]


def test_load_data():
    assert y.shape[1] == 36


def test_tokenize():
    test_str = 'I would like to know if one of the radio ginen Journalist died?'
    res = tokenize(test_str)
    assert res == ['i', 'would', 'like', 'to', 'know', 'if', 'one', 'of', 'the',
                   'radio', 'ginen', 'journalist', 'died']


def test_build_model():
    pipe = build_model()
    pipe.fit = MagicMock(return_value=(X_train, y_train))
    pipe.predict = MagicMock(return_value=X_test)
    assert pipe.fit.assert_called
    assert pipe.predict.assert_called

#
# def test_evaluate_model():
#     model = joblib.load('DisasterDataApp/models/classifier.pkl')
#     model.predict = MagicMock(return_value=X_test)
#     assert model.predict.assert_called
    # res = evaluate_model(model, X_test, y_test, col_names)
    # assert res == y_test

